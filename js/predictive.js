var app = angular.module('myApp1', []);

app.controller('myCtrl1', ['$scope','$http','$interval','$rootScope',function($scope,$http,$interval,$rootScope) {
    var link =  window.location.href
    var  link1= link.split('.html')[0];
    console.log( link.split('.html')[0])
    window.history.replaceState( null, null, link1 );

    $scope.predictivestart = function(){


        $scope.term = {
            name: 'Predictive'
        };
        $scope.measures = {
            name: 'rVoltage'
        };


        $scope.loader = {
            value: true
        };

        var selectedMeasure

        if($scope.measures.name === 'rVoltage')
        {
            selectedMeasure = 'R Voltage'
        }
        if($scope.measures.name === 'yVoltage'){
            selectedMeasure = 'Y Voltage'
        }
        if($scope.measures.name === 'bVoltage'){
            selectedMeasure = 'B Voltage'
        }



        $scope.headers = {
            description: selectedMeasure+' (in volts)'
        };

        /* $scope.Get = function (param){
             $scope.term = {
                 name: param
             };
         }*/
        $scope.GetValues = function (){

            if($scope.measures.name === 'rVoltage')
            {
                selectedMeasure = 'R Voltage'
            }
            if($scope.measures.name === 'yVoltage'){
                selectedMeasure = 'Y Voltage'
            }
            if($scope.measures.name === 'bVoltage'){
                selectedMeasure = 'B Voltage'
            }
            $scope.headers = {
                description:selectedMeasure+' (in volts)'
            };

            $.plot($("#bar-Chart"),
                [],{ grid  : {
                        borderWidth: 0,
                        borderColor: '#f3f3f3',
                        tickColor  : '#f3f3f3',
                    }

            }
            );

            // $('#bar-Chart').empty()

            var data=[]
            $scope.loader = {
                value: true
            };

            if($scope.term.name===''){
                alert("select EDA or PREDICTIVE")
            }
            else if($scope.measures.name===''){
                alert("select  Parameters")
            }
            else
            {
                console.log($scope.term.name)
                console.log($scope.measures.name)
                $http({
                    method : 'GET',
                    url : "http://10.9.15.153:2222/api/wipro/getPredictiveData/",
                    headers:{
                        'Content-Type': 'application/json',
                        'type' : $scope.term.name,
                        'feature': $scope.measures.name
                    }

                }).then(function(response) {
                    console.log("called")
                    $scope.loader = {
                        value: false
                    };




                    if(response.status === 200){


                        if($scope.term.name === 'Predictive'){



                            $scope.realtime=true




                            $scope.off = function (){
                                console.log("off method")
                                $scope.realtime= false
                                console.log($scope.realtime)
                            }



                            var actualValues=[]
                            var predictedValues=[]
                            var timestamps=[]
                            var predictionMetrics=[]

                            actualValues = response.data.actualValues
                            predictedValues=response.data.predictedValues
                            timestamps=response.data.timestamps
                            predictionMetrics=response.data.predictionMetrics
                            var ypfdata = [], Points = 300
                            var ypf,bpf,rpf
                            var bpfdata1=[]
                            var rpfdata2=[]
                            var avgdata
                            var Interval = 100;
                            var time
                            var j=0,x=0
                            function Data() {
                                ypfdata.shift();
                                while (ypfdata.length <= Points) {
                                    var temp = [timestamps[j],actualValues[j]];

                                    ypfdata.push(temp);
                                    j++
                                }

                                bpfdata1.shift();
                                while (bpfdata1.length <= Points) {
                                    var temp1 = [timestamps[x] ,predictedValues[x]];
                                    x++

                                    bpfdata1.push(temp1);
                                }
                            }

                            var optionsData = {
                                grid  : {
                                    borderColor: '#f3f3f3',
                                    borderWidth: 1,
                                    tickColor  : '#f3f3f3',
                                    /* markings: [{ yaxis: { from: 0.9, to: 0.9 }, color: "black" }]*/
                                },
                                series: {
                                    shadowSize: 0, // Drawing is faster without shadows
                                    color     : '#3c8dbc'
                                },
                                lines : {
                                    fill : false, //Converts the line chart to area chart
                                    color: '#3c8dbc'
                                },
                                xaxis: {
                                    mode: "time",

                                    tickFormatter: function (v, axis) {
                                        var date = new Date(v);
                                        var dt = date.toLocaleString()
                                        return dt
                                        /* if (date.getSeconds() % 20 == 0) {
                                             var hours = date.getHours() < 10 ? "0" + date.getHours() : date.getHours();
                                             var minutes = date.getMinutes() < 10 ? "0" + date.getMinutes() : date.getMinutes();
                                             var seconds = date.getSeconds() < 10 ? "0" + date.getSeconds() : date.getSeconds();

                                             return hours + ":" + minutes + ":" + seconds;
                                         } else {
                                             return "";
                                         }*/
                                    },
                                    axisLabel: "Time"+ ' (DD/MM/YYYY, HH:MM:SS)',
                                    axisLabelUseCanvas: true,
                                    axisLabelFontSizePixels: 12,
                                    axisLabelFontFamily: 'Verdana, Arial',
                                    axisLabelPadding:20
                                },
                                yaxis: {
                                    min : 225,
                                    max : 250,
                                    tickSize: 5,
                                    show: true
                                },
                                legend: {
                                  /*  labelBoxBorderColor: "#fff",
                                    position: "nw",
                                    noColumns: 2,
                                    margin: [0,-30]*/
                                    show: true
                                }
                            };

                            $(document).ready(function () {
                                Data();
                                var dataset = [
                                    {
                                        label: '<span style="padding-right:20px;">Actual Values</span>',
                                        data: ypfdata,
                                        color: 'red',
                                        lines: { show: true }
                                    },
                                    {
                                        label: '<span style="padding-right:20px;">Predicted Values</span>',
                                        data: bpfdata1,
                                        color:'black',
                                        lines: { show: true }
                                        /* dashes: { show: true },*/
                                    }

                                ];

                                $.plot($(".flotcontainer1"), dataset, optionsData);
                                function updateFun() {
                                    Data();
                                    $.plot($(".flotcontainer1"), dataset, optionsData)

                                    if($scope.realtime) {
                                        setTimeout(updateFun, Interval);
                                    }
                                }


                                    updateFun();

                                $scope.on = function (){
                                   if($scope.realtime === false) {
                                       $scope.realtime= true
                                       console.log($scope.realtime)
                                       updateFun();
                                   }

                                }




                            });


                            $scope.prediction = {
                                coefficientOfDetermination: predictionMetrics.coefficientOfDetermination,
                                feature:selectedMeasure,
                                meanAbsoluteError:predictionMetrics.meanAbsoluteError,
                                meanSquaredError:predictionMetrics.meanSquaredError

                            };



                        }

                    }
                })
            }

        }

        $scope.GetValues()

    };

    $scope.predictivestart();
}]);