var app = angular.module('myApp', []);

app.controller('myCtrl', ['$scope','$http','$interval','$rootScope',function($scope,$http,$interval,$rootScope) {

    var link =  window.location.href
 var  link1= link.split('.html')[0];
    console.log( link.split('.html')[0])
    window.history.replaceState( null, null, link1 );



    $scope.start = function(){


        $(".knob").knob({
            readOnly: true,
        });
        $(".knob1").knob({
            readOnly: true,
        });

        $(".knob2").knob({
            readOnly: true,
        });

        //api call for knob



        $interval(function() {

            $http({
                method : 'GET',
                url : "http://10.9.15.153:2222/api/wipro/getEnergySensorVoltageData/",
                headers:{
                    'Content-Type': 'application/json'
                }

            }).then(function(response) {
               /* console.log(response)*/
                $scope.voltage = {
                    bVoltage: response.data.bVoltage,
                    rVoltage:response.data.rVoltage,
                    yVoltage:response.data.yVoltage
                };


                /* jQuery Knob */

                $('.knob').val($scope.voltage.rVoltage).trigger('change');
                /* END JQUERY KNOB */

                /* jQuery Knob */

                $('.knob1').val($scope.voltage.yVoltage).trigger('change');
                /* END JQUERY KNOB */

                $('.knob2').val($scope.voltage.bVoltage).trigger('change');
                /* END JQUERY KNOB */
            })
        }, 1000);


        // Draw a sparkline for the #sparkline element
        var bVoltage
        var rVoltage
        var yVoltage
        // var chartMin = Math.min.apply(Math, values);
        // var chartMax = Math.max.apply(Math, values);
        var chartHeight = '95px';

        /*  $('#sparkline').sparkline(bVoltage, {
              type: "line",
              spotColor: false,
              height: chartHeight,
              fillColor:'blue',
              lineColor:'blue'
              // chartRangeMin: chartMin,
              // chartRangeMax: chartMax
          });

          $('#sparkline1').sparkline(rVoltage, {
              type: "line",
              spotColor: false,
              height: chartHeight,
              fillColor:'red',

              lineColor:'red,'
              // chartRangeMin: chartMin,
              // chartRangeMax: chartMax
          });
          $('#sparkline2').sparkline(yVoltage, {
              type: "line",
              spotColor: false,
              height: chartHeight,
              fillColor:'#DAA520',

              lineColor:'#DAA520,'
              // chartRangeMin: chartMin,
              // chartRangeMax: chartMax
          });
      */
        //api call for sparkline
        spark=   $interval(function() {
            $http({
                method : 'GET',
                url : "http://10.9.15.153:2222/api/wipro/getEnergySensorActivePowerData/",
                headers:{
                    'Content-Type': 'application/json'
                }

            }).then(function(response) {

                bVoltage=response.data.bActivePower
                rVoltage=response.data.rActivePower
                yVoltage=response.data.yActivePower

                $('#sparkline').sparkline(bVoltage, {
                    type: "line",
                    spotColor: false,
                    height: chartHeight,
                    fillColor:'blue',
                    lineColor:'blue',
                    width:100
                    // chartRangeMin: chartMin,
                    // chartRangeMax: chartMax
                });

                $('#sparkline1').sparkline(rVoltage, {
                    type: "line",
                    spotColor: false,
                    height: chartHeight,
                    fillColor:'red',
                    width:100,
                    lineColor:'red,'
                    // chartRangeMin: chartMin,
                    // chartRangeMax: chartMax
                });
                $('#sparkline2').sparkline(yVoltage, {
                    type: "line",
                    spotColor: false,
                    height: chartHeight,
                    fillColor:'#DAA520',
                    width:100,
                    lineColor:'#DAA520,'
                    // chartRangeMin: chartMin,
                    // chartRangeMax: chartMax
                });


            })
        }, 1000);


        $scope.related = {
            bRVoltage:0,
            rYVoltage:0,
            yBVoltage:0,
        };
        function dataLoad(param) {

            $scope.related = {
                bRVoltage: param.data.bRVoltage+'V',
                rYVoltage:param.data.rYVoltage+'V',
                yBVoltage:param.data.yBVoltage+'V',
            };
        }

        //api call for corelated data
        Correlated  =   $interval(function() {
            $http({
                method : 'GET',
                url : "http://10.9.15.153:2222/api/wipro/getEnergySensorCorrelatedVoltageData/",
                headers:{
                    'Content-Type': 'application/json'
                }

            }).then(function(response,status) {

                if(response.status === 200){
                    dataLoad(response)
                }

            })
        }, 1000);

        /*
         * Flot Interactive Chart
         * -----------------------
         */
        // We use an inline data source in the example, usually data would
        // be fetched from a server



        $scope.avg = {
            value: ''
        };

        var options
        var data = [], totalPoints = 100
        var y,b,r
        var data1=[]
        var data2=[]
        var avg
        var updateInterval = 300;
        var markings;
        //  var now = new Date().getTime();
        var now
        SensorCurrentData=      $interval(function() {
            $http({
                method : 'GET',
                url : "http://10.9.15.153:2222/api/wipro/getEnergySensorCurrentData/",
                headers:{
                    'Content-Type': 'application/json'
                }

            }).then(function(response) {

                y=response.data.yCurrent
                b=response.data.bCurrent
                r=response.data.rCurrent
                $scope.avg = {
                    value: response.data.averageCurrent
                };
                now=response.data.timestamp
                options = {
                    grid  : {
                        borderColor: '#f3f3f3',
                        borderWidth: 1,
                        tickColor  : '#f3f3f3',
                        markings: [{ yaxis: { from: $scope.avg.value, to:$scope.avg.value }, color: "black" }]

                    },
                    series: {
                        shadowSize: 0, // Drawing is faster without shadows
                        color     : '#3c8dbc'
                    },
                    lines : {
                        fill : false, //Converts the line chart to area chart
                        color: '#3c8dbc'
                    },
                    xaxis: {
                        mode: "time",
                        tickSize: [2, "second"],
                        tickFormatter: function (v, axis) {
                            var date = new Date(v);

                            if (date.getSeconds() % 20 == 0) {
                                var hours = date.getHours() < 10 ? "0" + date.getHours() : date.getHours();
                                var minutes = date.getMinutes() < 10 ? "0" + date.getMinutes() : date.getMinutes();
                                var seconds = date.getSeconds() < 10 ? "0" + date.getSeconds() : date.getSeconds();

                                return hours + ":" + minutes + ":" + seconds;
                            } else {
                                return "";
                            }
                        },
                        axisLabel: "Time",
                        axisLabelUseCanvas: true,
                        axisLabelFontSizePixels: 12,
                        axisLabelFontFamily: 'Verdana, Arial',
                        axisLabelPadding:20
                    },
                    yaxis: {

                        show: true,
                        tickFormatter: function (v, axis) {

                            return v + "A" ;

                        },
                    },
                    legend: {
                        labelBoxBorderColor: "#fff"
                    }
                };
            })
        }, 1000);

        function GetData() {
            data.shift();
            while (data.length < totalPoints) {
                //var temp = [now += updateInterval,y];
                var temp = [now ,y];
                data.push(temp);
            }

            data1.shift();
            while (data1.length < totalPoints) {
                var temp1 = [now ,b];
                data1.push(temp1);
            }

            data2.shift();
            while (data2.length < totalPoints) {
                var temp2 = [now ,r];
                data2.push(temp2);
            }


        }


        options = {
            grid  : {
                borderColor: '#f3f3f3',
                borderWidth: 1,
                tickColor  : '#f3f3f3',
                markings: [{ yaxis: { from: $scope.avg.value, to:$scope.avg.value }, color: "black" }]

            },
            series: {
                shadowSize: 0, // Drawing is faster without shadows
                color     : '#3c8dbc'
            },
            lines : {
                fill : false, //Converts the line chart to area chart
                color: '#3c8dbc'
            },
            xaxis: {
                mode: "time",
                tickSize: [2, "second"],
                tickFormatter: function (v, axis) {
                    var date = new Date(v);

                    if (date.getSeconds() % 20 == 0) {
                        var hours = date.getHours() < 10 ? "0" + date.getHours() : date.getHours();
                        var minutes = date.getMinutes() < 10 ? "0" + date.getMinutes() : date.getMinutes();
                        var seconds = date.getSeconds() < 10 ? "0" + date.getSeconds() : date.getSeconds();

                        return hours + ":" + minutes + ":" + seconds;
                    } else {
                        return "";
                    }
                },
                axisLabel: "Time",
                axisLabelUseCanvas: true,
                axisLabelFontSizePixels: 12,
                axisLabelFontFamily: 'Verdana, Arial',
                axisLabelPadding:20
            },
            yaxis: {
              /*  min : 1200,
                max : 1500,
                tickSize: 50,*/
                show: true,
                tickFormatter: function (v, axis) {

                    return v + "A" ;

                },
            },
            legend: {
                labelBoxBorderColor: "#fff"
            }
        };
        $(document).ready(function () {
            GetData();

            var dataset = [
                {
                    label:'Y Current',
                    data: data,
                    color: '#DAA520',
                    lines: { show: true }
                },
                {
                    label:'B Current',
                    data: data1,
                    color:'blue',
                    lines: { show: true }
                },
                {
                    label:'R Current',
                    data: data2,
                    color:'red',
                    lines: { show: true }
                }

            ];

            $.plot($("#flotcontainer"), dataset, options);
            function update() {
                GetData();

                $.plot($("#flotcontainer"), dataset, options)

                setTimeout(update, updateInterval);
            }

            update();
        });


        //power factor
        /*
             * Flot Interactive Chart
             * -----------------------
             */
        // We use an inline data source in the example, usually data would
        // be fetched from a server
        $scope.avg1 = {
            value: ''
        };

        var optionsData
        var ypfdata = [], Points = 100
        var ypf,bpf,rpf
        var bpfdata1=[]
        var rpfdata2=[]
        var avgdata
        var Interval = 300;
        var time
        PowerFactor=   $interval(function() {
            $http({
                method : 'GET',
                url : "http://10.9.15.153:2222/api/wipro/getEnergySensorPowerFactorData/",
                headers:{
                    'Content-Type': 'application/json'
                }

            }).then(function(response) {
                ypf=response.data.yPowerFactor
                bpf=response.data.bPowerFactor
                rpf=response.data.rPowerFactor
                time=response.data.timestamp

                $scope.avg1 = {
                    value: response.data.averagePowerFactor
                };


                optionsData = {
                    grid  : {
                        borderColor: '#f3f3f3',
                        borderWidth: 1,
                        tickColor  : '#f3f3f3',
                        markings: [{ yaxis: { from: $scope.avg1.value, to: $scope.avg1.value }, color: "black" }]
                    },
                    series: {
                        shadowSize: 0, // Drawing is faster without shadows
                        color     : '#3c8dbc'
                    },
                    lines : {
                        fill : false, //Converts the line chart to area chart
                        color: '#3c8dbc'
                    },
                    xaxis: {
                        mode: "time",
                        tickSize: [2, "second"],
                        tickFormatter: function (v, axis) {
                            var date = new Date(v);

                            if (date.getSeconds() % 20 == 0) {
                                var hours = date.getHours() < 10 ? "0" + date.getHours() : date.getHours();
                                var minutes = date.getMinutes() < 10 ? "0" + date.getMinutes() : date.getMinutes();
                                var seconds = date.getSeconds() < 10 ? "0" + date.getSeconds() : date.getSeconds();

                                return hours + ":" + minutes + ":" + seconds;
                            } else {
                                return "";
                            }
                        },
                        axisLabel: "Time",
                        axisLabelUseCanvas: true,
                        axisLabelFontSizePixels: 12,
                        axisLabelFontFamily: 'Verdana, Arial',
                        axisLabelPadding:20
                    },
                    yaxis: {
                       /* min : 0.5,
                        max : 1.10,
                        tickSize: 0.1,*/
                        show: true
                    },
                    legend: {
                        labelBoxBorderColor: "#fff"
                    }
                };

            })
        }, 1000);
        function Data() {
            ypfdata.shift();
            while (ypfdata.length < Points) {
                var temp = [time,ypf];
                ypfdata.push(temp);
            }

            bpfdata1.shift();
            while (bpfdata1.length < Points) {
                var temp1 = [time ,bpf];
                bpfdata1.push(temp1);
            }

            rpfdata2.shift();
            while (rpfdata2.length < Points) {
                var temp2 = [time , rpf];
                rpfdata2.push(temp2);
            }


        }

        optionsData = {
            grid  : {
                borderColor: '#f3f3f3',
                borderWidth: 1,
                tickColor  : '#f3f3f3',
                /* markings: [{ yaxis: { from: 0.9, to: 0.9 }, color: "black" }]*/
            },
            series: {
                shadowSize: 0, // Drawing is faster without shadows
                color     : '#3c8dbc'
            },
            lines : {
                fill : false, //Converts the line chart to area chart
                color: '#3c8dbc'
            },
            xaxis: {
                mode: "time",
                tickSize: [2, "second"],
                tickFormatter: function (v, axis) {
                    var date = new Date(v);

                    if (date.getSeconds() % 20 == 0) {
                        var hours = date.getHours() < 10 ? "0" + date.getHours() : date.getHours();
                        var minutes = date.getMinutes() < 10 ? "0" + date.getMinutes() : date.getMinutes();
                        var seconds = date.getSeconds() < 10 ? "0" + date.getSeconds() : date.getSeconds();

                        return hours + ":" + minutes + ":" + seconds;
                    } else {
                        return "";
                    }
                },
                axisLabel: "Time",
                axisLabelUseCanvas: true,
                axisLabelFontSizePixels: 12,
                axisLabelFontFamily: 'Verdana, Arial',
                axisLabelPadding:20
            },
            yaxis: {
               /* min : 0.5,
                max : 1.10,
                tickSize: 0.1,*/
                show: true,

            },
            legend: {
                labelBoxBorderColor: "#fff"
            }
        };

        $(document).ready(function () {
            Data();
            var dataset = [
                {
                    label:'Y Power Factor',
                    data: ypfdata,
                    color: '#DAA520',
                    lines: { show: true }
                },
                {
                    label:'B Power Factor',
                    data: bpfdata1,
                    color:'blue',
                    lines: { show: true }
                },
                {
                    label:'R Power Factor',
                    data: rpfdata2,
                    color:'red',
                    lines: { show: true }
                }

            ];

            $.plot($(".flotcontainer"), dataset, optionsData);
            function updateFun() {
                Data();
                $.plot($(".flotcontainer"), dataset, optionsData)
                setTimeout(updateFun, Interval);
            }

            updateFun();
        });


        //miscellaneous

        $scope.voltageData = {
            llVoltage:0+"V",
            activeEnergy:0+"kWh",
            eBPanelActiveEnergy:0+"kWh",
            lineToNeutralVoltage:0+'V',
            frequnecy:0+"Hz"
        };

        function dataLoadMiscellaneous(param) {

            $scope.voltageData = {
                llVoltage:param.data.lineToLineVoltage+"V",
                activeEnergy:param.data.activeEnergy+"kWh",
                eBPanelActiveEnergy:param.data.eBPanelActiveEnergy+"kWh",
                lineToNeutralVoltage:param.data.lineToNeutralVoltage+'V',
                frequnecy:param.data.frequnecy+"Hz",
                aestatus:param.data.activeEnergyStatus,
                ltlstatus:param.data.lineToLineVoltageStatus,
                ebstatus:param.data.eBPanelActiveEnergyStatus,
                ltnstatus:param.data.lineToNeutralVoltageStatus,
                frequnecyStatus:param.data.frequnecyStatus

            };


        }

        //api call for miscellaneous
        MiscellaneousData=    $interval(function() {
            $http({
                method : 'GET',
                url : "http://10.9.15.153:2222/api/wipro/getEnergySensorMiscellaneousData/",
                headers:{
                    'Content-Type': 'application/json'
                }

            }).then(function(response) {

                if(response.status === 200){
                    dataLoadMiscellaneous(response)

                }
            })
        }, 1000);
    };
    $scope.start();
    var PowerFactor
    var MiscellaneousData
    var SensorCurrentData
    var  Correlated
    var spark
    var voltage







    $scope.$on("MyFunction", function(){
        $scope.parentmethod();
    });
    $scope.parentmethod = function() {

        alert("calling")
        $interval.cancel(MiscellaneousData);
        $interval.cancel(Correlated);
        $interval.cancel(spark);
        $interval.cancel(SensorCurrentData);
        $interval.cancel(PowerFactor);
        $interval.cancel(voltage);
    }



}]);