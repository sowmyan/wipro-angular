var app = angular.module('myApp1', []);

app.controller('myCtrl1', ['$scope','$http','$interval','$rootScope',function($scope,$http,$interval,$rootScope) {
    var   history =[];

    $rootScope.$broadcast("MyFunction")
    $scope.descriptivestart = function(){
        var  message = []

        $scope.color = {
            name: 'Monthly'
        };
        $scope.description = {
            value: true
        };

        $scope.checked = [
            {
                id:1,
                Name: 'Voltage',
                Selected: false
            }, {
                id:2,
                Name: 'Active Power',
                Selected: false
            }, {
                id:3,
                Name: 'Current',
                Selected: false
            },{
                id:4,
                Name: 'Power Factor',
                Selected: false
            }];

        if(message.length===0){
            if( $scope.checked[0].Name === 'Voltage') {
                $scope.checked[0].Selected = true
                message.push("Voltage")
            }

        }


        $scope.heading = {
            name:  $scope.color.name+" Wise Data"
        };



        $scope.GetValue = function () {
            history =[];
            $scope.loader = {
                status: true
            };
            $scope.description = {
                value: false
            };


            message = [];
            for (var i = 0; i < $scope.checked.length; i++) {
                if ($scope.checked[i].Selected) {
                    var checkName = $scope.checked[i].Name;
                    message.push(checkName)

                }
            }
            if($scope.color.name===''){
                alert("select Quarterly or Monthly or Weekly")
            }
            else if(message.length===0){
                alert("select  Parameters")
            }
            else{

                loadPage()


            }

        }
        function loadPage(){
            $scope.heading = {
                name:  $scope.color.name+" Wise Data"
            };

            $scope.pushed = {
                status: false
            };

            //  $('#bar-chart').empty()
            $.plot($("#bar-chart"),
                [],{ grid  : {
                        borderWidth: 0,
                        borderColor: '#f3f3f3',
                        tickColor  : '#f3f3f3',
                    }}
            );



          /*  console.log(message);
            console.log($scope.color.name)*/
            $scope.loader = {
                status: true
            };

            $http({
                method : 'GET',
                url : "http://10.9.15.153:2222/api/wipro/getAggregationData/",
                headers:{
                    'Content-Type': 'application/json',
                    'term':$scope.color.name,
                    'measures':message
                }

            }).then(function(response) {





             /*   console.log(response)
                console.log(response.data[0].data.length)*/
                $scope.loader = {
                    status: false
                };
                console.log(history.length)

                //Quarterly data


                if($scope.color.name === 'Quarterly' &&  response.data[0].data.length === 4){

                    if(history.length === 0 ){

                        console.log("entered")
                        for(var i=0; i<response.data.length;i++ ){
                            history.push({
                                label: response.data[i].label,
                                data: response.data[i].data

                                /*color:getRandomColor()*/,
                            })
                        }

                        $scope.pushed = {
                            status: true
                        };

                    }
                }

                //Monthly data


                if($scope.color.name === 'Monthly' &&  response.data[0].data.length === 12){

                    if(history.length === 0 ){

                        console.log("entered")
                        for(var i=0; i<response.data.length;i++ ){
                            history.push({
                                label: response.data[i].label,
                                data: response.data[i].data

                                /*color:getRandomColor()*/,
                            })
                        }

                        $scope.pushed = {
                            status: true
                        };

                    }
                }
                //Weekly data


                if($scope.color.name === 'Weekly' &&  response.data[0].data.length === 5){

                    if(history.length === 0 ){

                        console.log("entered")
                        for(var i=0; i<response.data.length;i++ ){
                            history.push({
                                label: response.data[i].label,
                                data: response.data[i].data

                                /*color:getRandomColor()*/,
                            })
                        }

                        $scope.pushed = {
                            status: true
                        };

                    }
                }



                var ticks
                if(response.data[0].data.length === 12 ){
                    ticks= [[0, "January"], [1, "February"],[2,'March'],[3,'April'],[4,'May'], [5, "June"],[6,'July'],[7,'August'],[8,'September'],[9, "October"], [10, "November"],[11,'December']]

                }
                if(response.data[0].data.length === 4){
                    ticks= [[0, "Jan-Mar"], [1, "Apr-Jun"],[2,'Jul-Sep'],[3,'Oct-Dec']]

                }
                if(response.data[0].data.length === 5){
                    ticks= [[0, "Week 1"], [1, "Week 2"],[2,'Week 3'],[3,'Week 4'],[4,'Week 5']]

                }


                $.plot("#bar-chart",history, {
                    series: {
                        bars: {
                            show: true,
                            barWidth: 0.1,
                            align: "center",
                            order: 1
                        }
                    },
                    xaxis: {
                        mode: "categories",
                        tickLength: 0,
                        ticks:ticks
                    },
                    grid  : {
                        borderWidth: 1,
                        borderColor: '#f3f3f3',
                        tickColor  : '#f3f3f3',
                    }
                });





            })

            console.log("data",+ history)
        }


        loadPage()

        /*
         * BAR CHART
         * ---------
         */


        //geting Random colors

        function getRandomColor() {
            var letters = '0123456789ABCDEF';
            var color = '#';
            for (var i = 0; i < 6; i++) {
                color += letters[Math.floor(Math.random() * 16)];
            }
            return color;
        }


        /* END BAR CHART */


    };

    $scope.descriptivestart();

}]);