var http = require('http');
var fs = require("fs");

var mime = {
    html: 'text/html',
    txt: 'text/plain',
    css: 'text/css',
    gif: 'image/gif',
    jpg: 'image/jpeg',
    png: 'image/png',
    svg: 'image/svg+xml',
    js: 'application/javascript'
};


http.createServer(function(request, response) {

	if(request.url === "/index"){
		sendFileContent(response, "index.html", "text/html");
	}
  else if(request.url === "/predictive"){
        sendFileContent(response, "predictive.html", "text/html");
    }


	else if(request.url === "/"){
		response.writeHead(200, {'Content-Type': 'text/html'});
		response.write('<b>Hey there!</b><br /><br />This is the default response. Requested URL is: ' + request.url);
		response.end();
	}
	else if(/^\/[a-zA-Z0-9\/]*.js$/.test(request.url.toString())){
		sendFileContent(response, request.url.toString().substring(1), "text/javascript");
	}
	else if(/^\/[a-zA-Z0-9\/]*.css$/.test(request.url.toString())){
		sendFileContent(response, request.url.toString().substring(1), "text/css");
	}
	
	else{
		console.log("Requested URL is: " + request.url + ": " + request.url.toString().split(".").pop());
		var extension = request.url.toString().split(".").pop();
		var type = mime[extension] || 'text/plain';
		sendFileContent(response, request.url.toString().substring(1), type);
		
	}
}).listen(3000,function() {
	    console.log( 'Express server listening on port %d', 3000);
	});

function sendFileContent(response, fileName, contentType){
	console.log( "reading: " + fileName)
	fs.readFile(fileName, function(err, data){
		if(err){
			response.writeHead(404);
			response.write("Not Found!");
		}
		else{
			response.writeHead(200, {'Content-Type': contentType});
			response.write(data);
		}
		response.end();
	});
}

